import pyodbc
import pandas as pd
import sqlalchemy
import sys
from statsmodels.tsa.seasonal import seasonal_decompose
from statsmodels.tsa.holtwinters import SimpleExpSmoothing, ExponentialSmoothing
import time
import operator
from collections import defaultdict
import numpy as np
from tqdm import tqdm
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_squared_log_error
from sklearn.metrics import median_absolute_error
from sklearn.metrics import r2_score
import datetime as dt
from dateutil.relativedelta import relativedelta
from scipy.stats import variation
from math import sqrt

today = dt.date.today()
todayz = dt.datetime.today()

#Code for dynamically naming the columns at the bottom based on the month that the code is run
TodaysDate = 'FCS ' + time.strftime("%Y-%m")
TodaysDatez = time.strftime("%m")

six_beforez = today + relativedelta(months=-6)
five_beforez = today + relativedelta(months=-5)
four_beforez = today + relativedelta(months=-4)
three_beforez = today + relativedelta(months=-3)
two_beforez = today + relativedelta(months=-2)
one_beforez = today + relativedelta(months=-1)

six_before = 'HOF ' + six_beforez.strftime("%Y-%m")
five_before = 'HOF ' + five_beforez.strftime("%Y-%m")
four_before = 'HOF ' + four_beforez.strftime("%Y-%m")
three_before = 'HOF ' + three_beforez.strftime("%Y-%m")
two_before = 'HOF ' + two_beforez.strftime("%Y-%m")
one_before = 'HOF ' + one_beforez.strftime("%Y-%m")

#The ones that end in two zz's are for reasonalizing the data towards the bottom of the code.
six_beforezz = six_beforez.strftime("%m")
five_beforezz = five_beforez.strftime("%m")
four_beforezz = four_beforez.strftime("%m")
three_beforezz = three_beforez.strftime("%m")
two_beforezz = two_beforez.strftime("%m")
one_beforezz = one_beforez.strftime("%m")

one_afterz = today + relativedelta(months=+1)
two_afterz = today + relativedelta(months=+2)
three_afterz = today + relativedelta(months=+3)
four_afterz = today + relativedelta(months=+4)
five_afterz = today + relativedelta(months=+5)
six_afterz = today + relativedelta(months=+6)
seven_afterz = today + relativedelta(months=+7)
eight_afterz = today + relativedelta(months=+8)
nine_afterz = today + relativedelta(months=+9)
ten_afterz = today + relativedelta(months=+10)
eleven_afterz = today + relativedelta(months=+11)

one_after = 'FCS ' + one_afterz.strftime("%Y-%m")
two_after = 'FCS ' + two_afterz.strftime("%Y-%m")
three_after = 'FCS ' + three_afterz.strftime("%Y-%m")
four_after = 'FCS ' + four_afterz.strftime("%Y-%m")
five_after = 'FCS ' + five_afterz.strftime("%Y-%m")
six_after = 'FCS ' + six_afterz.strftime("%Y-%m")
seven_after = 'FCS ' + seven_afterz.strftime("%Y-%m")
eight_after = 'FCS ' + eight_afterz.strftime("%Y-%m")
nine_after = 'FCS ' + nine_afterz.strftime("%Y-%m")
ten_after = 'FCS ' + ten_afterz.strftime("%Y-%m")
eleven_after = 'FCS ' + eleven_afterz.strftime("%Y-%m")

one_afterzz = one_afterz.strftime("%m")
two_afterzz = two_afterz.strftime("%m")
three_afterzz = three_afterz.strftime("%m")
four_afterzz = four_afterz.strftime("%m")
five_afterzz = five_afterz.strftime("%m")
six_afterzz = six_afterz.strftime("%m")
seven_afterzz = seven_afterz.strftime("%m")
eight_afterzz = eight_afterz.strftime("%m")
nine_afterzz = nine_afterz.strftime("%m")
ten_afterzz = ten_afterz.strftime("%m")
eleven_afterzz = eleven_afterz.strftime("%m")

#Below is our connection function to Access that takes in a query, location of the database and the list that will save the information from the query

def access_connection(query, location, test_list):
    conn = pyodbc.connect(r'Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=%s' % (location)) 
    cursor = conn.cursor()
    cursor.execute(query)
    resultz = test_list
    columns = [column[0] for column in cursor.description]
    resultz.append(columns)
    for row in cursor.fetchall():
        resultz.append(row)
    conn.close()

    
#We will now call the function that pulls information from the database
database_location = 'customer_one.accdb'

#customer_one pos query
customer_one_pos_query = """SELECT customer_onePOS.customer_oneSKU, customer_onePOS.customer_onePART, customer_onePOS.customer_oneQTY, customer_onePOS.customer_oneYEAR, customer_onePOS.customer_oneWEEK, customer_onePOS.customer_onePD, customer_onePOS.companyYEAR, customer_onePOS.companyMONTH, customer_onePOS.companyPD
FROM `W:\Access Database\customer_one.accdb`.customer_onePOS customer_onePOS
WHERE (customer_onePOS.customer_oneQTY>0) AND (customer_onePOS.companyYEAR>2015)
ORDER BY customer_onePOS.customer_oneSKU"""
customer_one_pos_list = []

#customer_one_friction part list
customer_one_part_query = """
SELECT *
FROM `W:\Access Database\customer_one.accdb`.customer_oneITEM customer_oneITEM
ORDER BY customer_oneITEM.customer_oneSKU"""
customer_one_part_list = []
#customer_one calendar conversion where only difference is it uses year 2013 instead of 2014 like above
customer_one_calendar_conv_query = """SELECT CALENDARCONVERSIONFACTOR.companyMONTH, CALENDARCONVERSIONFACTOR.companyYEAR, CALENDARCONVERSIONFACTOR.companyPD, CALENDARCONVERSIONFACTOR.companyPDWEEKCOUNT, CALENDARCONVERSIONFACTOR.customer_onePDTOcompanyCONVERSION
FROM `W:\Access Database\customer_one.accdb`.CALENDARCONVERSIONFACTOR CALENDARCONVERSIONFACTOR
WHERE (CALENDARCONVERSIONFACTOR.companyYEAR>2013.0)"""
customer_one_calendar_conv_list = []

#Below we initialize all of the queries using the access connection function
access_connection(customer_one_pos_query, database_location, customer_one_pos_list)
access_connection(customer_one_part_query, database_location, customer_one_part_list)
access_connection(customer_one_calendar_conv_query, database_location, customer_one_calendar_conv_list)

#The function below does most of the basic forecasting models. It also calculates the hold out forecasts for 3MA,6MA,12MA,naive, and seasonal naive
def basic_forecasts(input_list, product, twelve_month_output_list, six_month_output_list, three_month_output_list, naive_output_list, seasonal_naive_output_list):
    data = input_list[1:]
    twelve_average = sum(data[-12:]) / 12
    twelve_six = sum(data[-18:-6]) / 12
    twelve_five = sum(data[-17:-5]) / 12
    twelve_four = sum(data[-16:-4]) / 12
    twelve_three = sum(data[-15:-3]) / 12
    twelve_two = sum(data[-14:-2]) / 12
    twelve_one = sum(data[-13:-1]) / 12
    twelve_month_output_list.append([product, '12MA', twelve_six, twelve_five, twelve_four, twelve_three, twelve_two, twelve_one, twelve_average, twelve_average, twelve_average, twelve_average, twelve_average, twelve_average, twelve_average, twelve_average, twelve_average, twelve_average, twelve_average, twelve_average])
    six_average = sum(data[-6:]) / 6
    six_six = sum(data[-12:-6]) / 6
    six_five = sum(data[-11:-5]) / 6
    six_four = sum(data[-10:-4]) / 6
    six_three = sum(data[-9:-3]) / 6
    six_two = sum(data[-8:-2]) / 6
    six_one = sum(data[-7:-1]) / 6
    six_month_output_list.append([product, '6MA', six_six, six_five, six_four, six_three, six_two, six_one, six_average, six_average, six_average, six_average, six_average, six_average, six_average, six_average, six_average, six_average, six_average, six_average])
    three_average = sum(data[-3:]) / 3
    three_six = sum(data[-9:-6]) / 3
    three_five = sum(data[-8:-5]) / 3
    three_four = sum(data[-7:-4]) / 3
    three_three = sum(data[-6:-3]) / 3
    three_two = sum(data[-5:-2]) / 3
    three_one = sum(data[-4:-1]) / 3
    three_month_output_list.append([product, '3MA', three_six, three_five, three_four, three_three, three_two, three_one, three_average, three_average, three_average, three_average, three_average, three_average, three_average, three_average, three_average, three_average, three_average, three_average])
    naive_output_list.append([product, 'Naive', data[-7], data[-6], data[-5], data[-4], data[-3], data[-2], data[-1], data[-1], data[-1], data[-1], data[-1], data[-1], data[-1], data[-1], data[-1], data[-1], data[-1], data[-1]])
    seasonal_naive_output_list.append([product, 'SeasonalNaive', data[-18], data[-17], data[-16], data[-15], data[-14], data[-13], data[-12], data[-11], data[-10], data[-9], data[-8], data[-7], data[-6], data[-5], data[-4], data[-3], data[-2], data[-1]])
    
#The function below is for simple exponential smoothing. It takes in three different smoothing factors and calculates the holdout forecast for each model
def simple_expo(input_list, product, forecast_length, forecast_length_two, output_list_alphaone, output_list_alphatwo, output_list_alphathree):
    data = input_list[1:]
    row_sum = sum(data[-12:])
    if row_sum > 0:
        model1 = SimpleExpSmoothing(data).fit(smoothing_level=0.1, optimized=False)
        fcast1 = model1.forecast(forecast_length)
        fcast_list1 = list(fcast1)
        fcast_list1.insert(0, product)
        fcast_list1.insert(1, 'SimpleExpo.1')
        ###Hold out forecast month 1
        model2 = SimpleExpSmoothing(data[:-6]).fit(smoothing_level=0.1, optimized=False)
        fcast2 = float(model2.forecast(forecast_length_two))
        fcast_list1.insert(2, fcast2)
        #######Hold out forecast month 2
        model3 = SimpleExpSmoothing(data[:-5]).fit(smoothing_level=0.1, optimized=False)
        fcast3 = float(model3.forecast(forecast_length_two))
        fcast_list1.insert(3, fcast3)
        #######Hold out forecast month 3
        model4 = SimpleExpSmoothing(data[:-4]).fit(smoothing_level=0.1, optimized=False)
        fcast4 = float(model4.forecast(forecast_length_two))
        fcast_list1.insert(4, fcast4)
        ######Hold out forecast month 4
        model5 = SimpleExpSmoothing(data[:-3]).fit(smoothing_level=0.1, optimized=False)
        fcast5 = float(model5.forecast(forecast_length_two))
        fcast_list1.insert(5, fcast5)
        ######Hold out forecast month 5
        model6 = SimpleExpSmoothing(data[:-2]).fit(smoothing_level=0.1, optimized=False)
        fcast6 = float(model6.forecast(forecast_length_two))
        fcast_list1.insert(6, fcast6)
        ######Hold out forecast month 6
        model7 = SimpleExpSmoothing(data[:-1]).fit(smoothing_level=0.1, optimized=False)
        fcast7 = float(model7.forecast(forecast_length_two))
        fcast_list1.insert(7, fcast7)
        output_list_alphaone.append(fcast_list1)
        ################
        model8 = SimpleExpSmoothing(data).fit(smoothing_level=0.2, optimized=False)
        fcast8 = model8.forecast(forecast_length)
        fcast_list8 = list(fcast8)
        fcast_list8.insert(0, product)
        fcast_list8.insert(1, 'SimpleExpo.2')
        ####
        model9 = SimpleExpSmoothing(data[:-6]).fit(smoothing_level=0.2, optimized=False)
        fcast9 = float(model2.forecast(forecast_length_two))
        fcast_list8.insert(2, fcast9)
        #######
        model10 = SimpleExpSmoothing(data[:-5]).fit(smoothing_level=0.2, optimized=False)
        fcast10 = float(model10.forecast(forecast_length_two))
        fcast_list8.insert(3, fcast10)
        #######
        model11 = SimpleExpSmoothing(data[:-4]).fit(smoothing_level=0.2, optimized=False)
        fcast11 = float(model11.forecast(forecast_length_two))
        fcast_list8.insert(4, fcast11)
        ######
        model12 = SimpleExpSmoothing(data[:-3]).fit(smoothing_level=0.2, optimized=False)
        fcast12 = float(model12.forecast(forecast_length_two))
        fcast_list8.insert(5, fcast12)
        ######
        model13 = SimpleExpSmoothing(data[:-2]).fit(smoothing_level=0.2, optimized=False)
        fcast13 = float(model13.forecast(forecast_length_two))
        fcast_list8.insert(6, fcast13)
        ######
        model14 = SimpleExpSmoothing(data[:-1]).fit(smoothing_level=0.2, optimized=False)
        fcast14 = float(model14.forecast(forecast_length_two))
        fcast_list8.insert(7, fcast14)
        output_list_alphatwo.append(fcast_list8)
        #######################################
        model15 = SimpleExpSmoothing(data).fit(smoothing_level=0.3, optimized=False)
        fcast15 = model15.forecast(forecast_length)
        fcast_list15 = list(fcast15)
        fcast_list15.insert(0, product)
        fcast_list15.insert(1, 'SimpleExpo.3')
        ######
        model16 = SimpleExpSmoothing(data[:-6]).fit(smoothing_level=0.3, optimized=False)
        fcast16 = float(model16.forecast(forecast_length_two))
        fcast_list15.insert(2, fcast16)
        #######
        model17 = SimpleExpSmoothing(data[:-5]).fit(smoothing_level=0.3, optimized=False)
        fcast17 = float(model17.forecast(forecast_length_two))
        fcast_list15.insert(3, fcast17)
        #######
        model18 = SimpleExpSmoothing(data[:-4]).fit(smoothing_level=0.3, optimized=False)
        fcast18 = float(model18.forecast(forecast_length_two))
        fcast_list15.insert(4, fcast18)
        ######
        model19 = SimpleExpSmoothing(data[:-3]).fit(smoothing_level=0.3, optimized=False)
        fcast19 = float(model19.forecast(forecast_length_two))
        fcast_list15.insert(5, fcast19)
        ######
        model20 = SimpleExpSmoothing(data[:-2]).fit(smoothing_level=0.3, optimized=False)
        fcast20 = float(model20.forecast(forecast_length_two))
        fcast_list15.insert(6, fcast20)
        ######
        model21 = SimpleExpSmoothing(data[:-1]).fit(smoothing_level=0.3, optimized=False)
        fcast21 = float(model21.forecast(forecast_length_two))
        fcast_list15.insert(7, fcast21)
        output_list_alphathree.append(fcast_list15)            
    elif row_sum == 0:
        output_list_alphaone.append([product, 'SimpleExpo.1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        output_list_alphatwo.append([product, 'SimpleExpo.2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        output_list_alphathree.append([product, 'SimpleExpo.3', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])

#Function below runs 9 different models as well as calculates the hold out forecast for each model(ie the different smoothing levels/smoothing slopes
def double_expo(input_list, product, forecast_length, forecast_length_two, output_list1, output_list2, output_list3, output_list4, output_list5, output_list6, output_list7, output_list8, output_list9):
    #data = np.asarray(input_list[1:])
    #data[data==0] = np.nan
    data = input_list[1:]
    row_sum = sum(data[-12:])
    if row_sum > 0:
        model1 = ExponentialSmoothing(data, trend='add', seasonal=None, damped=False).fit(smoothing_level=0.1, smoothing_slope=0.1, optimized=False)
        fcast1 = model1.forecast(forecast_length)
        fcast_list1 = list(fcast1)
        fcast_list1.insert(0, product)
        fcast_list1.insert(1, 'DoubleExpo1')
        #####
        model1_1 = ExponentialSmoothing(data[:-6], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.1, smoothing_slope=0.1, optimized=False)
        fcast1_1 = float(model1_1.forecast(forecast_length_two))
        fcast_list1.insert(2, fcast1_1)
        #####
        model1_2 = ExponentialSmoothing(data[:-5], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.1, smoothing_slope=0.1, optimized=False)
        fcast1_2 = float(model1_2.forecast(forecast_length_two))
        fcast_list1.insert(3, fcast1_2)
        #####
        model1_3 = ExponentialSmoothing(data[:-4], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.1, smoothing_slope=0.1, optimized=False)
        fcast1_3 = float(model1_3.forecast(forecast_length_two))
        fcast_list1.insert(4, fcast1_3)
        #####
        model1_4 = ExponentialSmoothing(data[:-3], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.1, smoothing_slope=0.1, optimized=False)
        fcast1_4 = float(model1_4.forecast(forecast_length_two))
        fcast_list1.insert(5, fcast1_4)
        #####
        model1_5 = ExponentialSmoothing(data[:-2], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.1, smoothing_slope=0.1, optimized=False)
        fcast1_5 = float(model1_5.forecast(forecast_length_two))
        fcast_list1.insert(6, fcast1_5)
        #####
        model1_6 = ExponentialSmoothing(data[:-1], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.1, smoothing_slope=0.1, optimized=False)
        fcast1_6 = float(model1_6.forecast(forecast_length_two))
        fcast_list1.insert(7, fcast1_6)
        output_list1.append(fcast_list1)
        ###################################################################################
        model2 = ExponentialSmoothing(data, trend='add', seasonal=None, damped=False).fit(smoothing_level=0.1, smoothing_slope=0.2, optimized=False)
        fcast2 = model2.forecast(forecast_length)
        fcast_list2 = list(fcast2)
        fcast_list2.insert(0, product)
        fcast_list2.insert(1, 'DoubleExpo2')
        #####
        model2_1 = ExponentialSmoothing(data[:-6], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.1, smoothing_slope=0.2, optimized=False)
        fcast2_1 = float(model2_1.forecast(forecast_length_two))
        fcast_list2.insert(2,fcast2_1)
        #####
        model2_2 = ExponentialSmoothing(data[:-5], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.1, smoothing_slope=0.2, optimized=False)
        fcast2_2 = float(model2_2.forecast(forecast_length_two))
        fcast_list2.insert(3,fcast2_2)
        #####
        model2_3 = ExponentialSmoothing(data[:-4], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.1, smoothing_slope=0.2, optimized=False)
        fcast2_3 = float(model2_3.forecast(forecast_length_two))
        fcast_list2.insert(4,fcast2_3)
        #####
        model2_4 = ExponentialSmoothing(data[:-3], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.1, smoothing_slope=0.2, optimized=False)
        fcast2_4 = float(model2_4.forecast(forecast_length_two))
        fcast_list2.insert(5,fcast2_4)
        #####
        model2_5 = ExponentialSmoothing(data[:-2], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.1, smoothing_slope=0.2, optimized=False)
        fcast2_5 = float(model2_5.forecast(forecast_length_two))
        fcast_list2.insert(6,fcast2_5)
        ######
        model2_6 = ExponentialSmoothing(data[:-1], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.1, smoothing_slope=0.2, optimized=False)
        fcast2_6 = float(model2_6.forecast(forecast_length_two))
        fcast_list2.insert(7,fcast2_6)
        output_list2.append(fcast_list2)
        ###################################################################
        model3 = ExponentialSmoothing(data, trend='add', seasonal=None, damped=False).fit(smoothing_level=0.1, smoothing_slope=0.3, optimized=False)
        fcast3 = model3.forecast(forecast_length)
        fcast_list3 = list(fcast3)
        fcast_list3.insert(0, product)
        fcast_list3.insert(1, 'DoubleExpo3')
        #####
        model3_1 = ExponentialSmoothing(data[:-6], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.1, smoothing_slope=0.3, optimized=False)
        fcast3_1 = float(model3_1.forecast(forecast_length_two))
        fcast_list3.insert(2,fcast3_1)
        #####
        model3_2 = ExponentialSmoothing(data[:-5], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.1, smoothing_slope=0.3, optimized=False)
        fcast3_2 = float(model3_2.forecast(forecast_length_two))
        fcast_list3.insert(3,fcast3_2)
        #####
        model3_3 = ExponentialSmoothing(data[:-4], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.1, smoothing_slope=0.3, optimized=False)
        fcast3_3 = float(model3_3.forecast(forecast_length_two))
        fcast_list3.insert(4,fcast3_3)
        #####
        model3_4 = ExponentialSmoothing(data[:-3], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.1, smoothing_slope=0.3, optimized=False)
        fcast3_4 = float(model3_4.forecast(forecast_length_two))
        fcast_list3.insert(5,fcast3_4)
        #####
        model3_5 = ExponentialSmoothing(data[:-2], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.1, smoothing_slope=0.3, optimized=False)
        fcast3_5 = float(model3_5.forecast(forecast_length_two))
        fcast_list3.insert(6,fcast3_5)
        #####
        model3_6 = ExponentialSmoothing(data[:-1], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.1, smoothing_slope=0.3, optimized=False)
        fcast3_6 = float(model3_6.forecast(forecast_length_two))
        fcast_list3.insert(7,fcast3_6)
        output_list3.append(fcast_list3)
        #######################################################
        model4 = ExponentialSmoothing(data, trend='add', seasonal=None, damped=False).fit(smoothing_level=0.2, smoothing_slope=0.1, optimized=False)
        fcast4 = model4.forecast(forecast_length)
        fcast_list4 = list(fcast4)
        fcast_list4.insert(0, product)
        fcast_list4.insert(1, 'DoubleExpo4')
        ######
        model4_1 = ExponentialSmoothing(data[:-6], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.2, smoothing_slope=0.1, optimized=False)
        fcast4_1 = float(model4_1.forecast(forecast_length_two))
        fcast_list4.insert(2,fcast4_1)
        ######
        model4_2 = ExponentialSmoothing(data[:-5], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.2, smoothing_slope=0.1, optimized=False)
        fcast4_2 = float(model4_2.forecast(forecast_length_two))
        fcast_list4.insert(3,fcast4_2)
        ######
        model4_3 = ExponentialSmoothing(data[:-4], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.2, smoothing_slope=0.1, optimized=False)
        fcast4_3 = float(model4_3.forecast(forecast_length_two))
        fcast_list4.insert(4,fcast4_3)
        ######
        model4_4 = ExponentialSmoothing(data[:-3], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.2, smoothing_slope=0.1, optimized=False)
        fcast4_4 = float(model4_4.forecast(forecast_length_two))
        fcast_list4.insert(5,fcast4_4)
        #####
        model4_5 = ExponentialSmoothing(data[:-2], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.2, smoothing_slope=0.1, optimized=False)
        fcast4_5 = float(model4_5.forecast(forecast_length_two))
        fcast_list4.insert(6,fcast4_5)
        ######
        model4_6 = ExponentialSmoothing(data[:-1], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.2, smoothing_slope=0.1, optimized=False)
        fcast4_6 = float(model4_6.forecast(forecast_length_two))
        fcast_list4.insert(7,fcast4_6)
        output_list4.append(fcast_list4)
        ############################################################
        model5 = ExponentialSmoothing(data, trend='add', seasonal=None, damped=False).fit(smoothing_level=0.2, smoothing_slope=0.2, optimized=False)
        fcast5 = model5.forecast(forecast_length)
        fcast_list5 = list(fcast5)
        fcast_list5.insert(0, product)
        fcast_list5.insert(1, 'DoubleExpo5')
        #####
        model5_1 = ExponentialSmoothing(data[:-6], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.2, smoothing_slope=0.2, optimized=False)
        fcast5_1 = float(model5_1.forecast(forecast_length_two))
        fcast_list5.insert(2,fcast5_1)
        #####
        model5_2 = ExponentialSmoothing(data[:-5], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.2, smoothing_slope=0.2, optimized=False)
        fcast5_2 = float(model5_2.forecast(forecast_length_two))
        fcast_list5.insert(3,fcast5_2)
        ######
        model5_3 = ExponentialSmoothing(data[:-4], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.2, smoothing_slope=0.2, optimized=False)
        fcast5_3 = float(model5_3.forecast(forecast_length_two))
        fcast_list5.insert(4,fcast5_3)
        #####
        model5_4 = ExponentialSmoothing(data[:-3], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.2, smoothing_slope=0.2, optimized=False)
        fcast5_4 = float(model5_4.forecast(forecast_length_two))
        fcast_list5.insert(5,fcast5_4)
        #####
        model5_5 = ExponentialSmoothing(data[:-2], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.2, smoothing_slope=0.2, optimized=False)
        fcast5_5 = float(model5_5.forecast(forecast_length_two))
        fcast_list5.insert(6,fcast5_5)
        #####
        model5_6 = ExponentialSmoothing(data[:-1], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.2, smoothing_slope=0.2, optimized=False)
        fcast5_6 = float(model5_6.forecast(forecast_length_two))
        fcast_list5.insert(7,fcast5_6)
        output_list5.append(fcast_list5)
        ########################################################
        model6 = ExponentialSmoothing(data, trend='add', seasonal=None, damped=False).fit(smoothing_level=0.2, smoothing_slope=0.3, optimized=False)
        fcast6 = model6.forecast(forecast_length)
        fcast_list6 = list(fcast6)
        fcast_list6.insert(0, product)
        fcast_list6.insert(1, 'DoubleExpo6')
        #####
        model6_1 = ExponentialSmoothing(data[:-6], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.2, smoothing_slope=0.3, optimized=False)
        fcast6_1 = float(model6_1.forecast(forecast_length_two))
        fcast_list6.insert(2,fcast6_1)
        #####
        model6_2 = ExponentialSmoothing(data[:-5], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.2, smoothing_slope=0.3, optimized=False)
        fcast6_2 = float(model6_2.forecast(forecast_length_two))
        fcast_list6.insert(3,fcast6_2)
        #####
        model6_3 = ExponentialSmoothing(data[:-4], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.2, smoothing_slope=0.3, optimized=False)
        fcast6_3 = float(model6_3.forecast(forecast_length_two))
        fcast_list6.insert(4,fcast6_3)
        #####
        model6_4 = ExponentialSmoothing(data[:-3], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.2, smoothing_slope=0.3, optimized=False)
        fcast6_4 = float(model6_4.forecast(forecast_length_two))
        fcast_list6.insert(5,fcast6_4)
        #####
        model6_5 = ExponentialSmoothing(data[:-2], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.2, smoothing_slope=0.3, optimized=False)
        fcast6_5 = float(model6_5.forecast(forecast_length_two))
        fcast_list6.insert(6,fcast6_5)
        #####
        model6_6 = ExponentialSmoothing(data[:-1], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.2, smoothing_slope=0.3, optimized=False)
        fcast6_6 = float(model6_6.forecast(forecast_length_two))
        fcast_list6.insert(7,fcast6_6)
        output_list6.append(fcast_list6)
        ###########################################################################
        model7 = ExponentialSmoothing(data, trend='add', seasonal=None, damped=False).fit(smoothing_level=0.3, smoothing_slope=0.1, optimized=False)
        fcast7 = model7.forecast(forecast_length)
        fcast_list7 = list(fcast7)
        fcast_list7.insert(0, product)
        fcast_list7.insert(1, 'DoubleExpo7')
        #####
        model7_1 = ExponentialSmoothing(data[:-6], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.3, smoothing_slope=0.1, optimized=False)
        fcast7_1 = float(model7_1.forecast(forecast_length_two))
        fcast_list7.insert(2,fcast7_1)
        #####
        model7_2 = ExponentialSmoothing(data[:-5], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.3, smoothing_slope=0.1, optimized=False)
        fcast7_2 = float(model7_2.forecast(forecast_length_two))
        fcast_list7.insert(3,fcast7_2)
        #####
        model7_3 = ExponentialSmoothing(data[:-4], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.3, smoothing_slope=0.1, optimized=False)
        fcast7_3 = float(model7_3.forecast(forecast_length_two))
        fcast_list7.insert(4,fcast7_3)
        #####
        model7_4 = ExponentialSmoothing(data[:-3], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.3, smoothing_slope=0.1, optimized=False)
        fcast7_4 = float(model7_4.forecast(forecast_length_two))
        fcast_list7.insert(5,fcast7_4)
        #####
        model7_5 = ExponentialSmoothing(data[:-2], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.3, smoothing_slope=0.1, optimized=False)
        fcast7_5 = float(model7_5.forecast(forecast_length_two))
        fcast_list7.insert(6,fcast7_5)
        #####
        model7_6 = ExponentialSmoothing(data[:-1], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.3, smoothing_slope=0.1, optimized=False)
        fcast7_6 = float(model7_6.forecast(forecast_length_two))
        fcast_list7.insert(7,fcast7_6)
        output_list7.append(fcast_list7)
        ###############################################################################
        model8 = ExponentialSmoothing(data, trend='add', seasonal=None, damped=False).fit(smoothing_level=0.3, smoothing_slope=0.2, optimized=False)
        fcast8 = model8.forecast(forecast_length)
        fcast_list8 = list(fcast8)
        fcast_list8.insert(0, product)
        fcast_list8.insert(1, 'DoubleExpo8')
        #####
        model8_1 = ExponentialSmoothing(data[:-6], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.3, smoothing_slope=0.2, optimized=False)
        fcast8_1 = float(model8_1.forecast(forecast_length_two))
        fcast_list8.insert(2,fcast8_1)
        #####
        model8_2 = ExponentialSmoothing(data[:-5], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.3, smoothing_slope=0.2, optimized=False)
        fcast8_2 = float(model8_2.forecast(forecast_length_two))
        fcast_list8.insert(3,fcast8_2)
        #####
        model8_3 = ExponentialSmoothing(data[:-4], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.3, smoothing_slope=0.2, optimized=False)
        fcast8_3 = float(model8_3.forecast(forecast_length_two))
        fcast_list8.insert(4,fcast8_3)
        #####
        model8_4 = ExponentialSmoothing(data[:-3], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.3, smoothing_slope=0.2, optimized=False)
        fcast8_4 = float(model8_4.forecast(forecast_length_two))
        fcast_list8.insert(5,fcast8_4)
        #####
        model8_5 = ExponentialSmoothing(data[:-2], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.3, smoothing_slope=0.2, optimized=False)
        fcast8_5 = float(model8_5.forecast(forecast_length_two))
        fcast_list8.insert(6,fcast8_5)
        #####
        model8_6 = ExponentialSmoothing(data[:-1], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.3, smoothing_slope=0.2, optimized=False)
        fcast8_6 = float(model8_6.forecast(forecast_length_two))
        fcast_list8.insert(7,fcast8_6)
        output_list8.append(fcast_list8)
        ######################################################################
        model9 = ExponentialSmoothing(data, trend='add', seasonal=None, damped=False).fit(smoothing_level=0.3, smoothing_slope=0.3, optimized=False)
        fcast9 = model9.forecast(forecast_length)
        fcast_list9 = list(fcast9)
        fcast_list9.insert(0, product)
        fcast_list9.insert(1, 'DoubleExpo9')
        #####
        model9_1 = ExponentialSmoothing(data[:-6], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.3, smoothing_slope=0.3, optimized=False)
        fcast9_1 = float(model9_1.forecast(forecast_length_two))
        fcast_list9.insert(2,fcast9_1)
        ######
        model9_2 = ExponentialSmoothing(data[:-5], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.3, smoothing_slope=0.3, optimized=False)
        fcast9_2 = float(model9_2.forecast(forecast_length_two))
        fcast_list9.insert(3,fcast9_2)
        #####
        model9_3 = ExponentialSmoothing(data[:-4], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.3, smoothing_slope=0.3, optimized=False)
        fcast9_3 = float(model9_3.forecast(forecast_length_two))
        fcast_list9.insert(4,fcast9_3)
        #####
        model9_4 = ExponentialSmoothing(data[:-3], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.3, smoothing_slope=0.3, optimized=False)
        fcast9_4 = float(model9_4.forecast(forecast_length_two))
        fcast_list9.insert(5,fcast9_4)
        #####
        model9_5 = ExponentialSmoothing(data[:-2], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.3, smoothing_slope=0.3, optimized=False)
        fcast9_5 = float(model9_5.forecast(forecast_length_two))
        fcast_list9.insert(6,fcast9_5)
        #####
        model9_6 = ExponentialSmoothing(data[:-1], trend='add', seasonal=None, damped=False).fit(smoothing_level=0.3, smoothing_slope=0.3, optimized=False)
        fcast9_6 = float(model9_6.forecast(forecast_length_two))
        fcast_list9.insert(7,fcast9_6)
        output_list9.append(fcast_list9)
    elif row_sum == 0:
        output_list1.append([product, 'DoubleExpo1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        output_list2.append([product, 'DoubleExpo2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        output_list3.append([product, 'DoubleExpo3', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        output_list4.append([product, 'DoubleExpo4', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        output_list5.append([product, 'DoubleExpo5', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        output_list6.append([product, 'DoubleExpo6', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        output_list7.append([product, 'DoubleExpo7', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        output_list8.append([product, 'DoubleExpo8', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        output_list9.append([product, 'DoubleExpo9', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])

#This function is used to calculate the wmape during the measure check towards the bottom       
def wmapess(actual, forecast):
    wmapes = (sum([abs(a-f) if a else 0 for a,f in zip(actual, forecast)]) / sum(actual)) * 100
    return wmapes

              
#customer_one will be first. We convert using the calendar conversion as well as create a new dictionary of monthly totals
customer_one_unconverted = []
converted_customer_one_numbers = []
for row in tqdm(customer_one_pos_list[1:], desc="Converting raw POS data"):
    customer_onesku = row[0]
    customer_onepart = row[1]
    quantity = float(row[2])
    companypd = str(row[8])
    for row in customer_one_calendar_conv_list[1:]:
        companyp_date = str(row[2])
        conversion = float(row[4])
        if companypd == companyp_date:
            converted_quantity = quantity * conversion
            converted_customer_one_numbers.append([customer_onepart, companypd, converted_quantity])
            customer_one_unconverted.append([customer_onepart, companypd, quantity])

#Create dictionary that sums sales for each month
monthlysales = {}
for row in converted_customer_one_numbers:
    companypd = row[1]
    quantity = row[2]
    if companypd in monthlysales:
        monthlysales[companypd] += quantity
    else:
        monthlysales[companypd] = quantity
#Convert sales and date back into a list and sort        
monthysales_list = [[p,d] for (p),d in monthlysales.items()]
sorted_monthlysales = sorted(monthysales_list, key=operator.itemgetter(0))

#Slicing so we only have 3 years worth of data
three_years_sales = sorted_monthlysales[:36]

#Summing sales for each year
total_sales_one = 0
for row in three_years_sales[:12]:
    sales = row[1]
    total_sales_one += sales

total_sales_two = 0    
for row in three_years_sales[12:24]:
    sales = row[1]
    total_sales_two += sales

total_sales_three = 0    
for row in three_years_sales[24:36]:
    sales = row[1]
    total_sales_three += sales

#Creating the si amount for each year
si_list = []    
for row in three_years_sales[:12]:
    date = row[0]
    sales = row[1]
    si = (1200 / total_sales_one) * sales
    si_list.append([date, si])
    
for row in three_years_sales[12:24]:
    date = row[0]
    sales = row[1]
    si = (1200 / total_sales_two) * sales
    si_list.append([date, si])
    
for row in three_years_sales[24:36]:
    date = row[0]
    sales = row[1]
    si = (1200 / total_sales_three) * sales
    si_list.append([date, si])

#Inserting the si number back into the three years sales list
for row in three_years_sales:
    date = row[0]
    for items in si_list:
        si = items[1]
        datez = items[0]
        if datez == date:
            row.insert(2, si)
            break

#summing the si number by month over three years. This sums all of the January's, February, March etc
si_total = {}
for row in three_years_sales:
    date = row[0][-2:]
    si = row[2]
    if date in si_total:
        si_total[date] += si
    else:
        si_total[date] = si       

#Convert back to list
si_date = [[p,d] for (p),d in si_total.items()]

#Calculating the ASI number
for row in si_date:
    date = row[0]
    total = row[1]
    asi = total / 3
    row.insert(2, asi)

#Calculating the three month moving average for each month
date01 = 0
date02 = 0
date03 = 0
date04 = 0
date05 = 0
date06 = 0
date07 = 0
date08 = 0
date09 = 0
date10 = 0
date11 = 0
date12 = 0    
for row in three_years_sales:
    date = row[0][-2:]
    si = row[2]
    if date == '01':
        date01 += si
        date02 += si
        date12 += si
    elif date == '02':
        date01 += si
        date02 += si
        date03 += si
    elif date == '03':
        date04 += si
        date02 += si
        date03 += si
    elif date == '04':
        date04 += si
        date05 += si
        date03 += si
    elif date == '05':
        date04 += si
        date05 += si
        date06 += si
    elif date == '06':
        date07 += si
        date05 += si
        date06 += si
    elif date == '07':
        date07 += si
        date08 += si
        date06 += si
    elif date == '08':
        date07 += si
        date08 += si
        date09 += si
    elif date == '09':
        date10 += si
        date08 += si
        date09 += si
    elif date == '10':
        date10 += si
        date11 += si
        date09 += si
    elif date == '11':
        date10 += si
        date11 += si
        date12 += si
    elif date == '12':
        date01 += si
        date11 += si
        date12 += si

sasi = [['01', date01], ['02', date02], ['03', date03], ['04', date04], ['05', date05], ['06', date06], ['07', date07], ['08', date08], ['09', date09], ['10', date10], ['11', date11], ['12', date12]]

#Inserting the sasi moving average back into the si_date list
for row in si_date:
    date = row[0]
    for items in sasi:
        datez = items[0]
        sasinumber = items[1]
        if datez == date:
            row.insert(3, (sasinumber / 9))
            break

#Deseasonalizing the data
seasonalized_customer_one_numbers = []
for row in converted_customer_one_numbers:
    part = row[0]
    real_date = row[1]
    quantity = row[2]
    date = row[1][-2:]
    for rowz in si_date:
        datez = rowz[0]
        sasi = rowz[3] / 100
        if datez == date:
            seasonalized_quantity = quantity / sasi
            seasonalized_customer_one_numbers.append([part, real_date, seasonalized_quantity])
            break
      
#Dedupe product and dates so we can add back all possible date values for each product            
prod_deduped = []
dates_deduped = []    
for row in tqdm(seasonalized_customer_one_numbers, desc="Deduping product and dates"):
   product = row[0]
   datez = row[1]
   if product not in prod_deduped:
       prod_deduped.append(product)
   if datez not in dates_deduped:
       dates_deduped.append(datez)

#Creating master list of all dates and products with a quantity of zero so when we add to the seasonalized numbers in the next for loop, it will not affect quantity       
product_date_combined = []
for product in tqdm(prod_deduped, desc="Creating master list of all dates and products"):
    for dates in dates_deduped:
        product_date_combined.append([product, dates, 0])

for row in product_date_combined:
    seasonalized_customer_one_numbers.append(row)
    customer_one_unconverted.append(row)

#We will now sum using a dictionary by product and date
result = defaultdict(int)
for p, d, q in tqdm(seasonalized_customer_one_numbers, desc='Summing by product and date to dedupe'):
    result[(p,d)] += q
    
#We now put the dictionary into a list and sort    
customer_one_deduped_list = [[p,d,q] for (p,d),q in result.items()]
sorted_customer_one_deduped_list = sorted(customer_one_deduped_list, key=operator.itemgetter(0,1))

#We will now sum using a dictionary by product and date for the unconverted numbers that we will use for calculating the forecast error
result_unconverted = defaultdict(int)
for p, d, q in tqdm(customer_one_unconverted, desc='Summing by product and date to dedupe'):
    result_unconverted[(p,d)] += q
    
#We now put the dictionary into a list and sort for the unconverted numbers that we will use for calculating the forecast error
customer_one_unconverted_deduped = [[p,d,q] for (p,d),q in result_unconverted.items()]
sorted_customer_one_deduped_unconverted = sorted(customer_one_unconverted_deduped, key=operator.itemgetter(0,1))

#We will now transform the data into something more easy to work with
customer_one_trans = defaultdict(list)
for product, dates, count in tqdm(sorted_customer_one_deduped_list, desc="Transforming into new format for customer_one"):
    customer_one_trans[product].append(count)

customer_one_transformed = [list((k, *v)) for k, v in customer_one_trans.items()]

#We will now transform the data into something more easy to work with for the unconverted numbers that we will use for calcualting the forecast error in a couple for loops below
customer_one_trans_unconverted = defaultdict(list)
for product, dates, count in tqdm(sorted_customer_one_deduped_unconverted, desc="Transforming into new format for customer_one"):
    customer_one_trans_unconverted[product].append(count)

customer_one_transformed_unconverted = [list((k, *v)) for k, v in customer_one_trans_unconverted.items()]

#Filtering out products that have been discontinued
new_customer_one_transformed = []
disco_list = []
for row in tqdm(customer_one_transformed, desc="Removing products that have been discontinued"):
    product = row[0]
    for rowz in customer_one_part_list:
        productz = rowz[1]
        disco = rowz[-2]
        start = rowz[-1]
        type = rowz[4]
        if productz == product and disco is None:
            new_customer_one_transformed.append(row)
            disco_list.append([product, type, disco, start])
            break
        elif productz == product and disco is not None and disco > todayz:
            new_customer_one_transformed.append(row)
            disco_list.append([product, type, disco, start])
            break

#Time to run the forecasts
twelve_month_out = []
six_month_out = []
three_month_out = []
naive_out = []
seasonal_naive_out = []
single_out_alphaone = []
single_out_alphatwo = []
single_out_alphathree = []
double_out_one = []
double_out_two = []
double_out_three = []
double_out_four = []
double_out_five = []
double_out_six = []
double_out_seven = []
double_out_eight = []
double_out_nine = []

for row in tqdm(new_customer_one_transformed, desc="Generating forecasts....grab a coffee"):
    product = row[0]
    basic_forecasts(row, product, twelve_month_out, six_month_out, three_month_out, naive_out, seasonal_naive_out)
    simple_expo(row, product, 12, 1, single_out_alphaone, single_out_alphatwo, single_out_alphathree)
    double_expo(row, product, 12, 1, double_out_one, double_out_two, double_out_three, double_out_four, double_out_five, double_out_six, double_out_seven, double_out_eight, double_out_nine)

#Reasonalized the data in the for loop below
output_lists = [twelve_month_out, six_month_out, three_month_out, naive_out, seasonal_naive_out, single_out_alphaone, single_out_alphatwo, single_out_alphathree, double_out_one, double_out_two, double_out_three, double_out_four, double_out_five, double_out_six, double_out_seven, double_out_eight, double_out_nine]

for forecasts in tqdm(output_lists, desc="Re-seasonalizing the data"):
    for row in tqdm(forecasts):
        #Defining the 12 month forecast as well as the six month ex post forecast
        month12 = row[-1]
        month11 = row[-2]
        month10 = row[-3]
        month9 = row[-4]
        month8 = row[-5]
        month7 = row[-6]
        month6 = row[-7]
        month5 = row[-8]
        month4 = row[-9]
        month3 = row[-10]
        month2 = row[-11]
        month = row[-12]
        month_neg_one = row[-13]
        month_neg_two = row[-14]
        month_neg_three = row[-15]
        month_neg_four = row[-16]
        month_neg_five = row[-17]
        month_neg_six = row[-18]
        #Giving each month position a unique month identifier
        month12z = int(eleven_afterzz)
        month11z = int(ten_afterzz)
        month10z = int(nine_afterzz)
        month9z = int(eight_afterzz)
        month8z = int(seven_afterzz)
        month7z = int(six_afterzz)
        month6z = int(five_afterzz)
        month5z = int(four_afterzz)
        month4z = int(three_afterzz)
        month3z = int(two_afterzz)
        month2z = int(one_afterzz)
        monthz = int(TodaysDatez)
        month_neg_onez = int(one_beforezz)
        month_neg_twoz = int(two_beforezz)
        month_neg_threez = int(three_beforezz)
        month_neg_fourz = int(four_beforezz)
        month_neg_fivez = int(five_beforezz) 
        month_neg_sixz = int(six_beforezz)
        for rowz in si_date:
            sasi = rowz[3] / 100
            datez = int(rowz[0])
            #If the date equals the unique identifier, then multiply the quantity actually in the cell by the sasi and append this number to the row position
            if datez == month12z:
                new_value_12 = month12 * sasi
                row[-1] = new_value_12
            elif datez == month11z:
                new_value_11 = month11 * sasi
                row[-2] = new_value_11
            elif datez == month10z:
                new_value_10 = month10 * sasi
                row[-3] = new_value_10
            elif datez == month9z:
                new_value_9 = month9 * sasi
                row[-4] = new_value_9
            elif datez == month8z:
                new_value_8 = month8 * sasi
                row[-5] = new_value_8
            elif datez == month7z:
                new_value_7 = month7 * sasi
                row[-6] = new_value_7
            elif datez == month6z:
                new_value_6 = month6 * sasi
                row[-7] = new_value_6
            elif datez == month5z:
                new_value_5 = month5 * sasi
                row[-8] = new_value_5
            elif datez == month4z:
                new_value_4 = month4 * sasi
                row[-9] = new_value_4
            elif datez == month3z:
                new_value_3 = month3 * sasi
                row[-10] = new_value_3
            elif datez == month2z:
                new_value_2 = month2 * sasi
                row[-11] = new_value_2
            elif datez == monthz:
                new_value_1 = month * sasi
                row[-12] = new_value_1
            if datez == month_neg_onez:
                new_value_neg_1 = month_neg_one * sasi
                row[-13] = new_value_neg_1
            elif datez == month_neg_twoz:
                new_value_neg_2 = month_neg_two * sasi
                row[-14] = new_value_neg_2
            elif datez == month_neg_threez:
                new_value_neg_3 = month_neg_three * sasi
                row[-15] = new_value_neg_3
            elif datez == month_neg_fourz:
                new_value_neg_4 = month_neg_four * sasi
                row[-16] = new_value_neg_4
            elif datez == month_neg_fivez:
                new_value_neg_5 = month_neg_five * sasi
                row[-17] = new_value_neg_5
            elif datez == month_neg_sixz:
                new_value_neg_6 = month_neg_six * sasi
                row[-18] = new_value_neg_6


#Time to calculate the accuracy of the forecasts
for forecasts in tqdm(output_lists, desc="Calculating the accuracy of the forecasts"):
    for row in tqdm(forecasts):
        product = row[0]
        six_list = row[2:8]
        for rowz in customer_one_transformed_unconverted:
            data = rowz[-12:]
            productz = rowz[0]
            actual_one = rowz[-6]
            actual_two = rowz[-5]
            actual_three = rowz[-4]
            actual_four = rowz[-3]
            actual_five = rowz[-2]
            actual_six = rowz[-1]
            actual_list = [actual_one, actual_two, actual_three, actual_four, actual_five, actual_six]
            actual_sum = actual_one + actual_two + actual_three + actual_four + actual_five + actual_six
            if productz == product and actual_sum > 0:
                cov = variation(data, axis=None)
                new_cov = np.array2string(cov)
                newest_cov = float(new_cov)
                wmape = wmapess(actual_list, six_list)
                mae = mean_absolute_error(actual_list, six_list)
                medianae = median_absolute_error(actual_list, six_list)
                #msle = mean_squared_log_error(actual_list, six_list)
                mse = mean_squared_error(actual_list, six_list)
                r2 = r2_score(actual_list, six_list)
                rmse = sqrt(mse)
                row.insert(2, wmape)
                row.insert(3, mae)
                row.insert(4, medianae)
                row.insert(5, mse)
                row.insert(6, rmse)
                row.insert(7, r2)
                row.insert(8, newest_cov)
                break
            elif productz == product and actual_sum == 0:
                row.insert(2, '')
                row.insert(3, '')
                row.insert(4, '')
                row.insert(5, '')
                row.insert(6, '')
                row.insert(7, '')
                row.insert(8, '')
                break
 

#Accounting for the three month lead time for DI products
for forecasts in tqdm(output_lists, desc="Accounting for DI Products"):
    for row in tqdm(forecasts):
        product = row[0]
        for rowz in disco_list:
            productz = rowz[0]
            type = rowz[1]
            if productz == product and type == 'DI':
                row[-12] = ''
                row[-11] = ''
                break
 
#Calculating which forecast to use for each product
productsz = [product_row[0] for product_row in output_lists[0]]
productsz_forecasts = {product: [] for product in productsz}

for forecasts in output_lists:
    for row in forecasts:
        productsz_forecasts[row[0]].append(row[2])
 
best_score_fore_product = {product: forecasts.index(min(forecasts)) for product, forecasts in productsz_forecasts.items()}

final_forecasts = []
for index, forecasts in tqdm(enumerate(output_lists), desc='Choosing best forecast for each product'):
    for product_row in tqdm(forecasts):
        if best_score_fore_product[product_row[0]] == index:
            final_forecasts.append(product_row) 

#Columns for the Excel Spreadsheet
Cols = ('customer_onePart', 'Method', 'WMAPE', 'MAE', 'MedianAE', 'MSE', 'RMSE', 'CoD', 'CoV', six_before, five_before, four_before, three_before, two_before, one_before, TodaysDate, one_after, two_after, three_after, four_after, five_after, six_after, seven_after, eight_after, nine_after, ten_after, eleven_after)
Cols_Forecast = ('customer_onePart', 'Method', 'WMAPE', 'MAE', 'MedianAE', 'MSE', 'RMSE', 'CoD', 'CoV', TodaysDate, one_after, two_after, three_after, four_after, five_after, six_after, seven_after, eight_after, nine_after, ten_after, eleven_after)

#Convert Final Forecast into same order as before
final_forecast_ordered = []
for row in tqdm(new_customer_one_transformed, desc='Converting back to the same product order'):
    product = row[0]
    for rowz in final_forecasts:
        productz = rowz[0]
        if productz == product:
            final_forecast_ordered.append(rowz)
            break

#All of the data that we want in the excel file

final_decision = pd.DataFrame(final_forecast_ordered)
sorted_customer_one_df = pd.DataFrame(sorted_customer_one_deduped_list)
rawcustomer_one_data = pd.DataFrame(new_customer_one_transformed)
disco_df = pd.DataFrame(disco_list)
twelve_df = pd.DataFrame(twelve_month_out)
six_df = pd.DataFrame(six_month_out)
three_df = pd.DataFrame(three_month_out)
naive_df = pd.DataFrame(naive_out)
seasonalnaive_df = pd.DataFrame(seasonal_naive_out)
singleone_df = pd.DataFrame(single_out_alphaone)
singletwo_df = pd.DataFrame(single_out_alphatwo)
singlethree_df = pd.DataFrame(single_out_alphathree)
doubleone_df = pd.DataFrame(double_out_one)
doubletwo_df = pd.DataFrame(double_out_two)
doublethree_df = pd.DataFrame(double_out_three)
doublefour_df = pd.DataFrame(double_out_four)
doublefive_df = pd.DataFrame(double_out_five)
doublesix_df = pd.DataFrame(double_out_six)
doubleseven_df = pd.DataFrame(double_out_seven)
doubleeight_df = pd.DataFrame(double_out_eight)
doublenine_df = pd.DataFrame(double_out_nine)

#Rounding the forecasts as well as the forecast accurary measurements
final_decision = final_decision.replace('', np.NaN)
final_decision.iloc[:,9:] = final_decision.iloc[:,9:].round(0)
final_decision.iloc[:,2:9]= final_decision.iloc[:,2:9].round(2)

#Replacing anything less than 0 in the forecasts with 0
for i in range(9,27): 
    col_name = final_decision.columns[i]  
    final_decision.loc[final_decision[col_name] < 0, col_name] = 0

#Dropping unneccessary columns
drop_columns = [9,10,11,12,13,14]
final_decision.drop(final_decision.columns[drop_columns], axis=1, inplace=True)

print("Exporting to an excel file......")
#Using writer to write multiple sheets to a single excel file
writer = pd.ExcelWriter('customer_oneForecast.xlsx', engine='xlsxwriter')
Disco_header = ["customer_onePart", "Type", "Disco Date", "Start Date"]


final_decision.to_excel(writer, sheet_name='Forecast', header=Cols_Forecast, index=False)
sorted_customer_one_df.to_excel(writer, sheet_name='RawForecastData', header=False, index=False)
rawcustomer_one_data.to_excel(writer, sheet_name='ForecastData', header=False, index=False)
disco_df.to_excel(writer, sheet_name='Discontinued', header=Disco_header, index=False)
twelve_df.to_excel(writer, sheet_name='TwelveMovingAverage', header=Cols, index=False)
six_df.to_excel(writer, sheet_name='SixMovingAverage', header=Cols, index=False)
three_df.to_excel(writer, sheet_name='ThreeMovingAverage', header=Cols, index=False)
naive_df.to_excel(writer, sheet_name='Naive', header=Cols, index=False)
seasonalnaive_df.to_excel(writer, sheet_name='SeasonalNaive', header=Cols, index=False)
singleone_df.to_excel(writer, sheet_name='SESAlpha1', header=Cols, index=False)
singletwo_df.to_excel(writer, sheet_name='SESAlpha2', header=Cols, index=False)
singlethree_df.to_excel(writer, sheet_name='SESAlpha3', header=Cols, index=False)
doubleone_df.to_excel(writer, sheet_name='DoubleExpo1', header=Cols, index=False)
doubletwo_df.to_excel(writer, sheet_name='DoubleExpo2', header=Cols, index=False)
doublethree_df.to_excel(writer, sheet_name='DoubleExpo3', header=Cols, index=False)
doublefour_df.to_excel(writer, sheet_name='DoubleExpo4', header=Cols, index=False)
doublefive_df.to_excel(writer, sheet_name='DoubleExpo5', header=Cols, index=False)
doublesix_df.to_excel(writer, sheet_name='DoubleExpo6', header=Cols, index=False)
doubleseven_df.to_excel(writer, sheet_name='DoubleExpo7', header=Cols, index=False)
doubleeight_df.to_excel(writer, sheet_name='DoubleExpo8', header=Cols, index=False)
doublenine_df.to_excel(writer, sheet_name='DoubleExpo9', header=Cols, index=False)

writer.save()      
